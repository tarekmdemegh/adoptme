import React from "react";
import { StyleSheet, Text, View } from "react-native";

export default function homeScreen() {
  return (
    <View style={styles.container}>
      <Text>homeScreen</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center",
  },
});

import React from "react";
import { View, StyleSheet } from "react-native";

const Spacer = ({ Children }) => {
  return <View style={styles.Spacer}>{Children}</View>;
};

const styles = StyleSheet.create({
  Spacer: {
    marginTop: 15,
  },
});

export default Spacer;

import React from "react";

import { createAppContainer, createSwitchNavigator } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";

import { createBottomTabNavigator } from "react-navigation-tabs";

import homeScreen from "./src/pages/home/index";
import FavoriteScreen from "./src/pages/favorite";
import NotificationScreen from "./src/pages/Notification";
import ProfilScreen from "./src/pages/profil";

const switcchNavigator = createSwitchNavigator({
  mainFlow: createBottomTabNavigator({
    Home: homeScreen,
    Notification: NotificationScreen,
    Favorite: FavoriteScreen,
    profil: ProfilScreen,
  }),
});
const App = createAppContainer(switcchNavigator);
export default () => {
  return <App />;
};
